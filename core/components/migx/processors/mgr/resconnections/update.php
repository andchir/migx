<?php

/**
 * XdbEdit
 *
 * Copyright 2010 by Bruno Perner <b.perner@gmx.de>
 *
 * This file is part of XdbEdit, for editing custom-tables in MODx Revolution CMP.
 *
 * XdbEdit is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * XdbEdit is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * XdbEdit; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA 
 *
 * @package xdbedit
 */
/**
 * Update and Create-processor for xdbedit
 *
 * @package xdbedit
 * @subpackage processors
 */
//if (!$modx->hasPermission('quip.thread_view')) return $modx->error->failure($modx->lexicon('access_denied'));


if (empty($scriptProperties['object_id'])) {
    $updateerror = true;
    $errormsg = $modx->lexicon('quip.thread_err_ns');
    return;
}

$config = $modx->migx->customconfigs;

$modx->setOption(xPDO::OPT_AUTO_CREATE_TABLES,$config['auto_create_tables']);

$classname = $config['classname'];

$saveTVs = false;

if ($modx->lexicon) {
    $modx->lexicon->load($packageName . ':default');
}

if (isset($scriptProperties['data'])) {
    $scriptProperties = array_merge($scriptProperties, $modx->fromJson($scriptProperties['data']));
}

$resource_id = $modx->getOption('resource_id', $scriptProperties, false);

$scriptProperties[$config['idfield_local']] = $resource_id;

switch ($scriptProperties['task']) {
    case 'publish':
        $object = $modx->getObject($classname, $scriptProperties['object_id']);
        $object->set('publishedon', time());
        $object->set('published', '1');
        $unpub = $object->get('unpub_date');
        if ($unpub < time()) {
            $object->set('unpub_date', null);
        }
        $object->save();
    break;
    case 'unpublish':
        $object = $modx->getObject($classname, $scriptProperties['object_id']);
        $object->set('unpublishedon', time());
        $object->set('published', '0');
        $object->set('unpublishedby', $modx->user->get('id')); //feld fehlt noch
        $pub = $object->get('pub_date');
        if ($pub < time()) {
            $object->set('pub_date', null);
        }
        $object->save();
    break;
    case 'delete':
        $object = $modx->getObject($classname, $scriptProperties['object_id']);
        $object->set('deletedon', time());
        $object->set('deleted', '1');
        $object->set('deletedby', $modx->user->get('id'));
        $object->save();
    break;
    case 'recall':
        $object = $modx->getObject($classname, $scriptProperties['object_id']);
        $object->set('deleted', '0');
        $object->save();
    break;
        
    default:
        
        if(!empty($config['includeTVs']) && !empty($config['includeTVList'])){
            $includeTVList_arr = explode(',',$config['includeTVList']);
            $tvs = $modx->getCollection('modTemplateVar', array('name:IN' => $includeTVList_arr));
            foreach($tvs as $tv){
                if(isset($scriptProperties[$tv->name])) $scriptProperties['tv'.$tv->id] = $scriptProperties[$tv->name];
            }
        }
        
        //create
        if ($scriptProperties['object_id'] == 'new') {
            
            $response = $modx->runProcessor('resource/create', $scriptProperties, array('processors_path' => $modx->getOption('core_path') . 'model/modx/processors/'));
            
            if ($response->isError()) {
                return $modx->error->failure($response->getMessage());
            }
            
        }
        //update
        else{
            
            if(!empty($config['includeTVs'])) $scriptProperties['tvs'] = 1;
            if(!empty($scriptProperties['object_id'])) $scriptProperties['id'] = $scriptProperties['object_id'];
            
            $response = $modx->runProcessor('resource/update', $scriptProperties, array('processors_path' => $modx->getOption('core_path') . 'model/modx/processors/'));
            
            if ($response->isError()) {
                return $modx->error->failure($response->getMessage());
            }
            
        }
        
    break;
}


return $modx->error->success();


?>
